all:

update:
	./gradlew dependencyUpdates

clean:
	./gradlew vaadinClean clean
	rm -rf node_modules tsconfig.json package.json package-lock.json types.d.ts vite.config.ts vite.generated.ts

.PHONY: update clean
