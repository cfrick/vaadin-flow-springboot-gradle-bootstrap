package net.ofnir.app

import com.vaadin.flow.component.Composite
import com.vaadin.flow.component.Key
import com.vaadin.flow.component.Text
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.page.AppShellConfigurator
import com.vaadin.flow.component.page.BodySize
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.RouterLayout
import com.vaadin.flow.spring.annotation.EnableVaadin
import com.vaadin.flow.theme.Theme
import com.vaadin.flow.theme.lumo.LumoUtility
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.stereotype.Service

@SpringBootApplication
@Import([
        VaadinConfiguration
])
class VaadinApplication {
    static void main(String[] args) {
        SpringApplication.run(VaadinApplication, args)
    }
}

@Configuration
@EnableVaadin
@Theme('app-theme')
@BodySize(height = "100%")
class VaadinConfiguration implements AppShellConfigurator {
}

@Service
class HelloService {
    String sayHello(String to) { "Hello, ${to ?: "World"}" }
}

class MainLayout extends Div implements RouterLayout {
    MainLayout() {
        addClassName(LumoUtility.Margin.MEDIUM)
    }
}

@Route(value = "", layout = MainLayout)
@SuppressWarnings('unused')
class MainView extends Composite<Div> {
    MainView(HelloService helloService) {
        TextField tf = new TextField().tap {
            setLabel("Please enter a name to greet")
            setPlaceholder("World")
            setId("name-input")
            focus()
        }
        Button b = new Button("Greet!").tap {
            addThemeVariants(ButtonVariant.LUMO_PRIMARY)
            setId("greet-button")
            addClickShortcut(Key.ENTER)
            addClickListener {
                content.add(new Div(new Text(helloService.sayHello(tf.value))))
                tf.tap {
                    focus()
                    clear()
                }
            }
        }
        content.tap {
            add(new H1("Greeting Service"))
            add(new FormLayout(tf, b).tap {
                responsiveSteps = [new FormLayout.ResponsiveStep("0px", 1)]
            })
        }
    }
}
