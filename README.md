# Groovy/Springboot/Vaadin Flow/Gradle Bootstrap

Bootstrap repo roughly following the current/stable versions of:

- Groovy
- Springboot
- Vaadin Flow
- Gradle
- E2E tests with Cypress

For older versions, check out the history.  Yet this is meant to be used
as a starter/template and therefor follows the newest versions.
